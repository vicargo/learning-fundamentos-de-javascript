/*
    Metodo bind(): Es un método de todas las funciones, el cual nos devuelve una función para ser invocada en cualquier momento. Se caracteriza por permitirnos definir el scope-contexto y por permitir establecer previamente los parametros

        const newFunction = fun.bind(contexto, primerParametro)// Establece el scope y el-los parametros de fun

        newFunction(segundoParametro) // Ejecuta fun pero con la caracteristica de que ya esta establecido el scope y los parametros. Igualmente nos permite enviarle más parametros a fun si es el caso

    Métodos call() y apply(): A diferencia de bidn(), call() y apply() ejecutan la función “padre” instantaneamente. De igual forma nos permiten establecer el scope y los parametros con la pequeña diferencia de que apply, recibe los parametros en un array.

        fun.call(contexto, primerParametro, segundoParametro)//Ejecuta fun en el scope establecido y con los parametros enviados

        fun.apply(contexto, [primerParametro, segundoParametro)//Ejecuta fun en el scope establecido y con los parametros enviados en el array
*/

const victor = {
    nombre: "Victor",
    apellido: "Arnau"
}

/*
    Al hacer this dentro de la funcion saludar hacemos referencia al objeto window. Una manera de pasar el objeto a la funcion sería haciendo uso de la función call 
*/
function saludar(veces, uppercase) {
    let str = `Hola ${this.nombre} ${this.apellido}!`;
    str = uppercase ? str.toUpperCase() : str;

    for (let i = 0; i < veces; i++) {
        console.log(str);
    }
}

/*
    Con la función call el primer parametro hacemos referencia al contexto, es decir donde esta this, y seguidamente los parametros de la funcion. 
*/

saludar.call(victor, 3, true)


/*
    La funcion apply es lo mismo que call, lo unico que les diferencia es la manera de pasar los argumentos. 
*/

saludar.apply(victor, [3, true])