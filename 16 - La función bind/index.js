class Toggable {
    constructor(el){
        this.el = el
        this.el.innerHTML = 'Off' //Aseguramos que el texto diga off
        this.activated = false
        this.onClick = this.onClick.bind(this)
        this.el.addEventListener('click', this.onClick)
    }

    onClick(){
        //Cambiar el estado interno (de off a on o on a off)
        this.activated = !this.activated //Switecheams entre true y false
        //llamar a toggleText
        this.toggleText()
    }

    toggleText(){
        //Cambiar el texto
        this.el.innerHTML = this.activated ? 'On' : 'Off'
    }
}

const button = document.getElementById('boton')

const miBoton = new Toggable(button)