# Platzom 

Platzom es un idioma inventado para el [Curso de Fundamentos de Javascript](https://platzi.com/js) de Platzi. 

## Descripción del idioma

- Si la palabra termina con "ar", se le quitan esas dos letras. 
- Si la palabra inicia con "Z", se le añade "pe" al final.
- Si la palabra traducida tiene 10 o más letras, se debe partir en dos por la mitad y unir con un guion medio.
- Si la palabra original es un palíndromo (capicua), nínguna regla anterior cuenta y se devuelve la misma palabara pero intercalando letras mayúsculas y minúsculas.


## Instalación 

```
npm install platzom
```

## Uso

import platzom from 'platzom'

platzom('programar'); //Program
platzom('zidane'); //Zidanepe
platzom('zarpar'); //Zarppe
platzom('abecedario'); //abece-dario
platzom('sometemos'); //SoMeTeMoS 

## Creditos

- [Victor Arnau] (https://gitlab.com/vicargo)

## Licencia

[MIT](https://opensource.org/licenses/MIT)
