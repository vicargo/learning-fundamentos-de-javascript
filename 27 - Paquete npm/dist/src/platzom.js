'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = platzom;
/*
    Para importarlo:
        import { platzom } from 'platzom' //importa la funcion plazom del módulo platzom
        

        Al añadirle el default no tenemos que ponerle las llaves por que lo que se exporta por default es esa función

        import platzom from 'platzom'


    */
function platzom(str) {
    /* 
        Cuando ejecutamos funciones o metodos sobre los strings, estos no se modifican. 
          str.toUpperCase(); -> No se esta modificando, necesitamos asignarlo a una variable para poder modificarlo. 
    */
    var translation = str;

    /* 
        Regla 1 - Si la palabra termina con "ar", se le quitan esas dos letras.
          - Comprobamos que acabe en 'ar' con la funcion 'endsWith' de javascript
        - Asignamos el nuevo valor a translation haciendo uso de la funcion slice de javascript (ver doc)
      */

    if (str.toLowerCase().endsWith('ar')) {
        translation = str.slice(0, -2); //Desde el inicio hasta el final menos dos carácteres.
    }

    /* 
        Regla 2 - Si la palabra inicia con "Z", se le añade "pe" al final.
          - Primero convertimos la palabra a minúscula
        - Comprobamos que empiece con "z" con la función 'startsWith' de javascript 
        - Una vez cumpla la condición, le añadimos "pe" al final del string
        
    */

    if (str.toLowerCase().startsWith('z')) {
        translation += 'pe'; //translation = translation + 'pe';
    }

    /* 
        Regla 3 - Si la palabra TRADUCIDA tiene 10 o más letras, se debe partir en dos por la mitad y unir con un guion medio.
          - Asignamos constante a la longitud de la palabra y la longitud máxima permitida. 
        - Comprobamos si tiene mas de 10 carácteres.
        - Dividimos la palabra en dos. Utilizamos Math.round para que nos redondee el resultado en caso de que la longitud sea impar. 
        - Guardamos mitades en variables y las concatenamos en el resultado. 
        
    */

    var maxLength = 10;
    var strLength = translation.length;

    if (strLength >= maxLength) {
        var firstHalf = translation.slice(0, Math.round(strLength / 2)); //Del principio a la mitad
        var secondHalf = translation.slice(Math.round(strLength / 2)); //De la mitad al final, si no ponemos segundo parametro a slice es hasta el final.

        translation = firstHalf + '-' + secondHalf;
    }

    /* 
        Regla 4 - Si la palabra original es un palíndromo (capicua), nínguna regla anterior cuenta y se devuelve la misma palabara pero intercalando letras mayúsculas y minúsculas.
          - Lo primero que hacemos es comprobar si la palabra se lee igual por delante que por detrás.
        - Para ellos crearemos una función que dara la vuelta a nuestra palabra. 
            - Esta función primero convertirá nuestro string en un array dividiendo por letras. split('') que nos convierte el string en un array.;
            - Damos la vuelta al array para colocar la palabra de manera inversa reverse()
            - Convertimos de array a string nuevamente join(''). 
        - En caso de cumplirse la condición, es decir, que sean capicua, tenemos que intercalar mayúsuculas y minúsculas.
        - Para ello creamos otra función que se encargará de esto
            - Obtenemos length
            - Creamos una variable local para el resultado
            - Creamos una variable para preguntar si el valor es true o false para cambiarlo. 
        - Ponemos como primera regla para ganar en rendiemiento. 
    */

    function minMay(str) {
        var length = str.length;
        var translation = '';
        var capitalize = true;
        for (var i = 0; i < length; i++) {
            var char = str.charAt(i);
            translation += capitalize ? char.toUpperCase() : char.toLowerCase();
            capitalize = !capitalize;
        }

        return translation;
    }

    function reverse(str) {
        return str.split('').reverse().join('');
    }

    console.log(translation);
    return translation;
}